//
//  ViewController.swift
//  MySmartCam
//
//  Created by Walter de Back on 30.07.17.
//  Copyright © 2017 Walter de Back. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    /* capture image from device camera */
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    
    var sessionOutput = AVCapturePhotoOutput()
    
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var image: UIImage?

    /* convnet model */
    //let model = SqueezeNet()
    let model = Inceptionv3()
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        startRunningCaptureSession()
    }
    
    //https://www.youtube.com/watch?v=NNKPbdT9gXU
    func sceneLabel( forImage image:UIImage) -> Dictionary<String, Double>?{
        
        let thumbnail = resizeImage(image: image, newWidth: 299)
        print("sceneLabel: thumbnail: ", thumbnail)
        if let buffer = convertImageToPixelBuffer(from: thumbnail){
            let t1 = mach_absolute_time()
            guard let label = try? model.prediction(image: buffer) else {fatalError("Unexpected runtime error")}
            let t2 = mach_absolute_time()
            // print time of execution
            let elapsed = t2 - t1
            var timeBaseInfo = mach_timebase_info_data_t()
            mach_timebase_info(&timeBaseInfo)
            let elapsedNano = elapsed * UInt64(timeBaseInfo.numer) / UInt64(timeBaseInfo.denom);
            print("========== Execution time = \(elapsedNano / 1_000_000) msec ============")
            
            return label.classLabelProbs
            
        }
        return nil
    }
    
    // https://stackoverflow.com/questions/31966885/ios-swift-resize-image-to-200x200pt-px
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        // crop the height to make it into a square
        let cropHeight = (newHeight - newWidth) / 2.0
        let rect = CGRect(x: 0, y: cropHeight/2.0, width: newWidth, height: newWidth-cropHeight)
        let size = CGSize(width: newWidth, height: newWidth)
        
        UIGraphicsBeginImageContext(size)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    // convert UIImage to CVPixelBuffer
    // https://stackoverflow.com/questions/44462087/how-to-convert-a-uiimage-to-a-cvpixelbuffer
    func convertImageToPixelBuffer(from image: UIImage) -> CVPixelBuffer? {
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer : CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(image.size.width), Int(image.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        guard (status == kCVReturnSuccess) else {
            return nil
        }
        
        CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: pixelData, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)
        
        context?.translateBy(x: 0, y: image.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        
        UIGraphicsPushContext(context!)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        
        return pixelBuffer
    }
    

    func setupCaptureSession(){
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice(){
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(__deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        
        let devices = deviceDiscoverySession.devices
        print(devices)

        for device in devices{
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            }
            else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
            
        }
        currentCamera = backCamera
        print("currentCamera: ", currentCamera )
    }
    
    func setupInputOutput(){
        do{
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(captureDeviceInput)
            //sessionOutput = AVCapturePhotoOutput()
            sessionOutput.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
            captureSession.addOutput(sessionOutput)
        }
        catch{
            print(error)
        }
    }
    
    func setupPreviewLayer(){
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspect
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraPreviewLayer?.frame = self.view.frame
        self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
    }
    
    func startRunningCaptureSession(){
        captureSession.startRunning()
        
/*        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            // Put your code which should be executed with a delay here
            
            print("1")
            print("2: sleep")
            sleep(1)
            print("3: image: ", self.image)

            let labels_dict = self.sceneLabel(forImage: self.image!)
            print("4")
            var sortedLabels = Array(labels_dict!.keys)
            sortedLabels.sort { (o1, o2) -> Bool in
                return labels_dict![o1]! > labels_dict![o2]!
            }
            print("5")
            print("!! label = ", sortedLabels[0])
            self.label1.text = sortedLabels[0]
            print("!! label = ", sortedLabels[1])
            self.label2.text = sortedLabels[1]
            print("!! label = ", sortedLabels[0])
            self.label3.text = sortedLabels[2]
            
        }
*/
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func button_TouchUpInside(){
        
        let settings = AVCapturePhotoSettings()
        self.sessionOutput.capturePhoto(with: settings, delegate: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoto_Segue"{
            let previewCV = segue.destination as! PreviewViewController

            // pass image
            previewCV.image = self.image
            
            // perform prediction
            let labels_dict = self.sceneLabel(forImage: self.image!)
            
            // pass labels
            previewCV.classlabels = labels_dict
        }
    }

}

extension ViewController: AVCapturePhotoCaptureDelegate{
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        print("Delegate called")
        if let imageData = photo.fileDataRepresentation(){
            print(imageData)
            self.image = UIImage(data : imageData)
            print("Image set by delegate")
            performSegue(withIdentifier: "showPhoto_Segue", sender: nil)
        }
    }
}
