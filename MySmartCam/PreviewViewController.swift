//
//  PreviewViewController.swift
//  MySmartCam
//
//  Created by Walter de Back on 30.07.17.
//  Copyright © 2017 Walter de Back. All rights reserved.
//

import UIKit
import AVFoundation

class PreviewViewController: UIViewController {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    var image: UIImage!
    var classlabels: Dictionary<String, Double>!
    
    let speechSynthesizer = AVSpeechSynthesizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("!! image = ", self.image)
        //self.photo.image = UIImage(named: (""))
        self.photo.image = self.image
        
        var sortedLabels = Array(classlabels.keys)
        sortedLabels.sort { (o1, o2) -> Bool in
            return classlabels[o1]! > classlabels[o2]!
        }
        print("!! label = ", sortedLabels[0])
        self.label1.text = sortedLabels[0]
        print("!! label = ", sortedLabels[1])
        self.label2.text = sortedLabels[1]
        print("!! label = ", sortedLabels[0])
        self.label3.text = sortedLabels[2]

        speak(text: sortedLabels[0])
    }
    
    func speak(text: String) {
        let speechUtterance = AVSpeechUtterance(string: text)
        
        speechUtterance.rate = 0.50
        speechUtterance.pitchMultiplier = 0.50
        speechUtterance.volume = 1.0
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")

        speechSynthesizer.speak(speechUtterance)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButton_TouchUpInside(_ sender: Any) {
        print("Cancel")
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
